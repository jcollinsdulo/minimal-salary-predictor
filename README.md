This minimal project predicts salary based on years of experience.

# Data
Data is the Salary_Data.csv

# Model
Model is trained and tested in model.py file

# Server
All the reqs for flask to manage API in server.py

# Requests
request.py contains the code to process POST requests to server.

# Runs on Terminal
First run model.py, then followed by server.py and finally request.py. Each file is run on a different terminal.